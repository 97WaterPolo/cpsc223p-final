#890457906
#Alexander John Sigler
#Problem 1

from tkinter import *

''' Callback function to generate our triangle on button click '''
def createTriangle():
    rows = triangleSize.get()

    # creates triangle
    for i in range(rows + 1):
        tFrame = Frame(win)
        tFrame.pack()

        for j in range(i):
            label = Label(tFrame, text=i, bg='gray', fg='black', font="Helvetica 18 bold")
            label.pack(side=LEFT)


win = Tk()
win.title("Final Exam Prob. 1")
frame = Frame(win)
frame.pack()

# creates entry and submit button and puts it in the top frame
topframe = Frame(win)
topframe.pack()



''' Creates our input for the Triangle '''
triangleSize = IntVar()
entry = Entry(topframe, textvariable=triangleSize, bd=5)
entry.pack(side=LEFT)

'''  Creates our submission button for the triangle '''
submit = Button(topframe, text="SUBMIT", command=createTriangle)
submit.pack(side=RIGHT)

win.mainloop()
