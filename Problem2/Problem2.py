#890457906
#Alexander John Sigler
#Problem 2

from Customer import Customer
import pandas as pd
import math

customers = list()
productList = set()

''' Open the customer.txt and orders.txt and load values into our variables'''
file = open("customer.txt", "r")  # Open the file in read only mode
for x in file.readlines():
    row = x.split(";")
    customers.append(Customer(row[0].split(":")[1].strip(), row[1].split(":")[1].strip(), row[2].split(":")[1].strip()))

file = open("orders.txt", "r")  # Open the file in read only mode
for x in file.readlines():
    row = x.split(";")
    custId = row[0].split(":")[1].strip()
    product = row[1].split(":")[1].strip()
    amt = row[2].split(":")[1].strip()
    productList.add(product)  # Add the product name to our list

    for c in customers:
        if c.id == custId:
            c.addProductPurchase(product, int(amt))

''' This is to ensure every customer has a value for the different '''
for productName in productList:
    for c in customers:
        c.addProductPurchase(str(productName), 0)

productList = sorted(productList, key=lambda x: x.lower())  # Sorts the products alphabetically

''' Create our dataframe and populate our table '''
dataEntryList = list()
for c in customers:
    dataEntryList.append(c.getDataEntry())
df = pd.DataFrame(dataEntryList, columns=['Customer ID', 'Gender', 'Age'] + productList)


''' Calculate our correlation coefficients, equation
 can be found at https://www.statisticshowto.com/probability-and-statistics/correlation-coefficient-formula/
 '''
def correlationBetweenProducts(dataframe, productOneName, productTwoName):
    summedTotal = dict()
    summedSquaredTotal = dict()
    for productName in productList:

        newSum = 0
        newSquaredSum = 0

        for customerRow in dataframe.iterrows():
            newSum += customerRow[1][productName]
            newSquaredSum += customerRow[1][productName] ** 2

        summedTotal[productName] = newSum
        summedSquaredTotal[productName] = newSquaredSum

    product1 = summedTotal[productOneName]
    product2 = summedTotal[productTwoName]
    product1Squared = summedSquaredTotal[productOneName]
    product2Squared = summedSquaredTotal[productTwoName]
    numOfCustomers = len(dataframe.index)

    sumOfProduct1And2 = 0
    for customerRow in dataframe.iterrows():
        sumOfProduct1And2 += customerRow[1][productOneName] * customerRow[1][productTwoName]

    numerator = (numOfCustomers * sumOfProduct1And2) - (product1 * product2)
    denom1 = (numOfCustomers * product1Squared) - product1 ** 2
    denom2 = (numOfCustomers * product2Squared) - product2 ** 2
    denom = math.sqrt(denom1 * denom2)

    return 0 if denom == 0 else numerator / denom


''' Testing of our DataFrame and Coefficiencts '''

selectedProductOne = "Muffins"
selectedProductTwo = "Oreos"

print("\nCorrelation Coefficient between " + selectedProductOne + " and " + selectedProductTwo + ": " + str(
    correlationBetweenProducts(df, selectedProductOne, selectedProductTwo)))
print(df)
print("Using DF's Correlation function: ")
print(df[[selectedProductOne, selectedProductTwo]].corr())

print("\nCorrelation Coefficient between " + selectedProductOne + " and " + selectedProductTwo + " for MALE: " + str(
    correlationBetweenProducts(df[df.Gender == "M"], selectedProductOne, selectedProductTwo)))
print(df[df.Gender == "M"])
print("Using DF's Correlation function: ")
print(df[df.Gender == "M"][[selectedProductOne, selectedProductTwo]].corr())

print("\nCorrelation Coefficient between " + selectedProductOne + " and " + selectedProductTwo + " for FEMALE: " + str(
    correlationBetweenProducts(df[df.Gender == "F"], selectedProductOne, selectedProductTwo)))
print(df[df.Gender == "F"])
print("Using DF's Correlation function: ")
print(df[df.Gender == "F"][[selectedProductOne, selectedProductTwo]].corr())
