#890457906
#Alexander John Sigler
#Problem 2 Resource file


class Customer:

    def __init__(self, id, gender, age):
        self.__id = id
        self.__gender = gender
        self.__age = age
        self.__products = dict()

    @property
    def id(self):
        return self.__id

    @property
    def gender(self):
        return self.__gender

    @property
    def age(self):
        return self.__age

    ''' Add a product to the customer, if it already exists increase amount. '''

    def addProductPurchase(self, productName, productAmount):
        if productName in self.__products:
            self.__products[productName] += productAmount
        else:
            self.__products[productName] = int(productAmount)

    def getProductCount(self, productName):
        if productName in self.__products:
            return self.__products[productName]

    def getDataEntry(self):
        productsInOrder = list()
        sortednames = sorted(self.__products.keys(), key=lambda x: x.lower())
        for key in sortednames:
            productsInOrder.append(self.__products[key])
        return [self.__id, self.__gender, self.__age] + productsInOrder

    def printOut(self):
        msg = "CustID: " + self.__id + " Gender: " + self.__gender + " Age: " + self.__age + " Purchased: "
        sortednames = sorted(self.__products.keys(), key=lambda x: x.lower())
        for key in sortednames:
            msg += key + "," + str(self.__products[key]) + ","
        return msg
